package io.seanthecoder.qmog
package http.routes

import Misc.testJsonFile
import codecs.ErrorBodyCodec.errorBodyEntityDecoder
import fixtures.MovieFixture.movies
import http.Routes
import models.{Movie, RequestError}
import services.MovieCollection

import cats.effect.IO
import cats.implicits.toShow
import munit.CatsEffectSuite
import org.http4s.{Method, Request, Response, Status, Uri}

class MovieRoutesSpec extends CatsEffectSuite {
  movieCollectionResponseFixture(fileName = MovieCollection.moviesFileName)
    .test(name = "route /movie/all should return the prod movie collection") {
      movieCollectionResponse =>
        val (movieCollection, response) = movieCollectionResponse
        import movieCollection.movieCodec.moviesEntityDecoder

        assertIO(response.map(_.status), Status.Ok)
        assertIO(response.flatMap(_.as[List[Movie]]).map(_.size), returns = 24)
    }

  movieCollectionResponseFixture()
    .test(name = "route /movie/all should return the test movie collection") {
      movieCollectionResponse =>
        val (movieCollection, response) = movieCollectionResponse
        import movieCollection.movieCodec.moviesEntityDecoder

        assertIO(response.map(_.status), Status.Ok)
        assertIO(response.flatMap(_.as[List[Movie]]), movies)
    }

  testGetMovie(itemId = 0)
  testGetMovie(itemId = 1)
  testGetMovie(itemId = 2)

  movieCollectionResponseFixture(itemId = "999")
    .test(name = "route /movie/999 should return 404 with error message") {
      movieCollectionResponse =>
        val response = movieCollectionResponse._2
        assertIO(response.map(_.status), Status.NotFound)
        assertIO(response.flatMap(_.as[RequestError]), RequestError("Movie with ID 999 not found"))
    }

  movieCollectionResponseFixture(itemId = "Christmas")
    .test(name = "route /movie/Christmas should return 400 with error message") {
      movieCollectionResponse =>
        val response = movieCollectionResponse._2
        assertIO(response.map(_.status), Status.BadRequest)
        assertIO(response.flatMap(_.as[RequestError]), RequestError("Invalid Movie ID - For input string: \"Christmas\""))
    }

  private def testGetMovie(itemId: Int): Unit =
    movieCollectionResponseFixture(itemId = itemId.show)
      .test(name = s"route /movie/$itemId should return movie with ID $itemId from the test movie collection") {
        movieCollectionResponse =>
          val (movieCollection, response) = movieCollectionResponse
          import movieCollection.movieCodec.movieEntityDecoder

          assertIO(response.map(_.status), Status.Ok)
          assertIO(response.flatMap(_.as[Movie]).map(_.title), movies(itemId).title)
      }

  private def movieCollectionResponseFixture(
    itemId: String = "all",
    fileName: String = testJsonFile,
  ): FunFixture[(MovieCollection[IO], IO[Response[IO]])] =
    FunFixture(
      setup = _ => {
        val movieCollection = MovieCollection[IO](fileName)
        val endpointUri     = Uri.unsafeFromString(s"/movie/$itemId")
        val request         = Request[IO](Method.GET, endpointUri)
        val httpApp         = Routes[IO].movieRoutes(movieCollection).orNotFound
        val response        = httpApp.run(request)
        (movieCollection, response)
      },
      teardown = movieCollectionResponse => movieCollectionResponse._1.resetId(),
    )
}
