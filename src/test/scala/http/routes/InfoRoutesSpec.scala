package io.seanthecoder.qmog
package http.routes

import codecs.InfoCodec.infoEntityDecoder
import fixtures.InfoFixture.{info, infoJson, infoLocationHeader}
import http.Routes
import models.Info
import services.ServerInfo

import cats.effect.IO
import cats.implicits.catsSyntaxOptionId
import munit.CatsEffectSuite
import org.http4s.circe.toMessageSyntax
import org.http4s.headers.Location
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s.{Method, Request, Response, Status, Uri}

class InfoRoutesSpec extends CatsEffectSuite {
  test(name = "route /info should return API server info") {
    assertIO(infoResponse().map(_.status), Status.Ok)
    assertIO(infoResponse().flatMap(_.as[Info]), info)
    assertIO(infoResponse().flatMap(_.asJson), infoJson)
  }

  test(name = "route / should redirect to /info") {
    assertIO(infoResponse(uri = uri"/").map(_.status), Status.TemporaryRedirect)
    assertIO(infoResponse(uri = uri"/").map(_.headers.get[Location]), infoLocationHeader.some)
  }

  private def infoResponse(uri: Uri = uri"/info"): IO[Response[IO]] = {
    val httpApp = Routes[IO].infoRoutes(ServerInfo[IO]).orNotFound
    val request = Request[IO](Method.GET, uri)
    httpApp.run(request)
  }
}
