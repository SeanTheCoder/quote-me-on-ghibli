package io.seanthecoder.qmog
package http.routes

import Misc.testJsonFile
import fixtures.CategoryFixture.categories
import http.Routes
import models.Category
import services.MovieCollection

import cats.effect.IO
import munit.CatsEffectSuite
import org.http4s.{Method, Request, Response, Status, Uri}

class CategoryRoutesSpec extends CatsEffectSuite {
  movieCollectionResponseFixture()
    .test(name = "route /category/all should return categories") {
      movieCollectionResponse =>
        val (movieCollection, response) = movieCollectionResponse
        import movieCollection.movieCodec.categoryCodec.categoriesEntityDecoder

        assertIO(response.map(_.status), Status.Ok)
        assertIO(response.flatMap(_.as[List[Category]]), categories)
    }

  private def movieCollectionResponseFixture(
    itemId: String = "all",
    fileName: String = testJsonFile,
  ): FunFixture[(MovieCollection[IO], IO[Response[IO]])] =
    FunFixture(
      setup = _ => {
        val movieCollection = MovieCollection[IO](fileName)
        val endpointUri     = Uri.unsafeFromString(s"/category/$itemId")
        val request         = Request[IO](Method.GET, endpointUri)
        val httpApp         = Routes[IO].categoryRoutes(movieCollection).orNotFound
        val response        = httpApp.run(request)
        (movieCollection, response)
      },
      teardown = movieCollectionResponse => movieCollectionResponse._1.resetId(),
    )
}
