package io.seanthecoder.qmog
package http.routes

import codecs.ErrorBodyCodec.errorBodyEntityDecoder
import http.Routes
import models.RequestError

import cats.effect.IO
import cats.implicits.catsSyntaxOptionId
import munit.CatsEffectSuite
import org.http4s.headers.{`Content-Length`, `Content-Type`}
import org.http4s.{MediaType, Method, Request, Response, Status, Uri}

class ImageAssetRoutesSpec extends CatsEffectSuite {
  responseFixture(Uri.unsafeFromString("/image/profile/1-nausicaa.jpg"))
    .test(name = "/image/profile/1-nausicaa.jpg should return image file") {
      response =>
        assertIO(response.map(_.status), Status.Ok)
        assertIO(response.map(_.headers.get[`Content-Type`]), `Content-Type`(MediaType.image.jpeg).some)
        assertIO(response.map(_.headers.get[`Content-Length`].fold(false)(70000L < _.length)), returns = true)
    }

  responseFixture(Uri.unsafeFromString("/image/profile/non-existent.jpg"))
    .test(name = "/image/profile/non-existent.jpg should return 404 with error message") {
      response =>
        assertIO(response.map(_.status), Status.NotFound)
        assertIO(response.flatMap(_.as[RequestError]), RequestError("Movie profile with name non-existent.jpg not found"))
    }

  private def responseFixture(endpointUri: Uri): FunFixture[IO[Response[IO]]] =
    FunFixture(
      setup = _ => {
        val request  = Request[IO](Method.GET, endpointUri)
        val httpApp  = Routes[IO].imageAssetRoutes.orNotFound
        val response = httpApp.run(request)
        response
      },
      teardown = _ => (),
    )
}
