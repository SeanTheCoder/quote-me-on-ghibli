package io.seanthecoder.qmog
package http.routes

import codecs.HealthCodec.healthEntityDecoder
import fixtures.HealthFixture.{health, healthJson}
import http.Routes
import models.Health
import services.HealthCheck

import cats.effect.IO
import munit.CatsEffectSuite
import org.http4s.circe.toMessageSyntax
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s.{Method, Request, Response, Status}

class HealthRoutesSpec extends CatsEffectSuite {
  test(name = "route /health should return health status") {
    assertIO(healthResponse.map(_.status), Status.Ok)
    assertIO(healthResponse.flatMap(_.as[Health]), health)
    assertIO(healthResponse.flatMap(_.asJson), healthJson)
  }

  private lazy val healthResponse: IO[Response[IO]] = {
    val httpApp = Routes[IO].healthRoutes(HealthCheck[IO]).orNotFound
    val request = Request[IO](Method.GET, uri = uri"/health")
    httpApp.run(request)
  }
}
