package io.seanthecoder.qmog
package http.routes

import Misc.testJsonFile
import codecs.ErrorBodyCodec.errorBodyEntityDecoder
import fixtures.MovieFixture.{movies, moviesWithCategory1}
import http.Routes
import models.{Movie, RequestError}
import services.MovieCollection

import cats.effect.IO
import cats.implicits.{catsSyntaxOptionId, none}
import munit.CatsEffectSuite
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s.{Method, Request, Response, Status, Uri}

class SearchRoutesSpec extends CatsEffectSuite {
  movieCollectionResponseFixture()
    .test(name = "route /search should return empty list without any query parameter") {
      movieCollectionResponse =>
        val (movieCollection, response) = movieCollectionResponse
        import movieCollection.movieCodec.moviesEntityDecoder

        assertIO(response.map(_.status), Status.Ok)
        assertIO(response.flatMap(_.as[List[Movie]]), Nil)
    }

  movieCollectionResponseFixture(keyword = "test-title".some)
    .test(name = "route /search should return all that match keyword \"test-title\"") {
      movieCollectionResponse =>
        val (movieCollection, response) = movieCollectionResponse
        import movieCollection.movieCodec.moviesEntityDecoder

        assertIO(response.map(_.status), Status.Ok)
        assertIO(response.flatMap(_.as[List[Movie]]), movies)
    }

  movieCollectionResponseFixture(categoryId = "1".some)
    .test(name = "route /search should return all that match category name \"1\"") {
      movieCollectionResponse =>
        val (movieCollection, response) = movieCollectionResponse
        import movieCollection.movieCodec.moviesEntityDecoder

        assertIO(response.map(_.status), Status.Ok)
        assertIO(response.flatMap(_.as[List[Movie]]), moviesWithCategory1)
    }

  movieCollectionResponseFixture(categoryId = "non-numeric".some)
    .test(name = "route /search should return 400 with category ID \"non-numeric\"") {
      movieCollectionResponse =>
        val response = movieCollectionResponse._2
        assertIO(response.map(_.status), Status.BadRequest)
        assertIO(response.flatMap(_.as[RequestError]), RequestError("Invalid Category ID - Illegal embedded sign character"))
    }

  movieCollectionResponseFixture(categoryName = "test-category-1".some)
    .test(name = "route /search should return all that match category name \"test-category-1\"") {
      movieCollectionResponse =>
        val (movieCollection, response) = movieCollectionResponse
        import movieCollection.movieCodec.moviesEntityDecoder

        assertIO(response.map(_.status), Status.Ok)
        assertIO(response.flatMap(_.as[List[Movie]]), moviesWithCategory1)
    }

  movieCollectionResponseFixture(keyword = "test-summary-2".some, categoryId = "4".some, categoryName = "test-category-0".some)
    .test(name = "route /search should return all that match keyword, category ID or category name") {
      movieCollectionResponse =>
        val (movieCollection, response) = movieCollectionResponse
        import movieCollection.movieCodec.moviesEntityDecoder

        assertIO(response.map(_.status), Status.Ok)
        assertIO(response.flatMap(_.as[List[Movie]]), movies)
    }

  private def movieCollectionResponseFixture(
    keyword: Option[String] = none,
    categoryId: Option[String] = none,
    categoryName: Option[String] = none,
  ): FunFixture[(MovieCollection[IO], IO[Response[IO]])] =
    FunFixture(
      setup = _ => {
        val endpointUri = Uri().withPath(path = path"/movie/search")
          .withOptionQueryParam(key = "keyword", keyword)
          .withOptionQueryParam(key = "categoryId", categoryId)
          .withOptionQueryParam(key = "categoryName", categoryName)

        val movieCollection = MovieCollection[IO](testJsonFile)
        val request         = Request[IO](Method.GET, endpointUri)
        val httpApp         = Routes[IO].movieRoutes(movieCollection).orNotFound
        val response        = httpApp.run(request)
        (movieCollection, response)
      },
      teardown = movieCollectionResponse => movieCollectionResponse._1.resetId(),
    )
}
