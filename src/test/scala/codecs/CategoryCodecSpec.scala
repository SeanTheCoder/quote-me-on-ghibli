package io.seanthecoder.qmog
package codecs

import models.Category

import cats.implicits.catsSyntaxEitherId
import io.circe.Json
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import munit.FunSuite

class CategoryCodecSpec extends FunSuite {
  private lazy val categoryCodec = CategoryCodec()
  import categoryCodec.{categoryDecoder, categoryEncoder, nextId, resetId}

  FunFixture.map2(categoryFixture, jsonFixture).test(name = "categoryEncoder should encode object to JSON") {
    case (category, json) =>
      assertEquals(category.asJson(categoryEncoder), json)
      assertEquals(nextId, BigInt(0)) // Default initial ID.
  }

  FunFixture.map2(jsonStringFixture, categoryFixture).test(name = "categoryDecoder should decode string to object") {
    case (jsonString, category) =>
      assertEquals(decode(jsonString)(categoryDecoder), category.asRight)
      assertEquals(nextId, BigInt(1)) // Auto incremented ID for the next.
  }

  test(name = "categoryDecoder should decode JSON strings with auto incremented IDs") {
    val jsonString10 = """{ "id": 10, "name": "test-category-10" }"""
    val jsonString11 = """{ "id": 11, "name": "test-category-11" }"""
    val jsonString12 = """{ "id": 12, "name": "test-category-12" }"""

    val jsonStringWithoutId10 = """{ "name": "test-category-10" }"""
    val jsonStringWithoutId11 = """{ "name": "test-category-11" }"""
    val jsonStringWithoutId12 = """{ "name": "test-category-12" }"""

    val JsonStringWithoutId13 = """{ "name": "test-category-no-id-13" }"""
    val JsonStringWithoutId14 = """{ "name": "test-category-no-id-14" }"""
    val JsonStringWithoutId15 = """{ "name": "test-category-no-id-15" }"""

    // Category JSON strings with IDs.
    assertEquals(decode(jsonString10)(categoryDecoder), Category(BigInt(10), "test-category-10").asRight)
    assertEquals(decode(jsonString11)(categoryDecoder), Category(BigInt(11), "test-category-11").asRight)
    assertEquals(decode(jsonString12)(categoryDecoder), Category(BigInt(12), "test-category-12").asRight)
    assertEquals(nextId, BigInt(13))

    // Category JSON strings without IDs.
    assertEquals(decode(JsonStringWithoutId13)(categoryDecoder), Category(BigInt(13), "test-category-no-id-13").asRight)
    assertEquals(decode(JsonStringWithoutId14)(categoryDecoder), Category(BigInt(14), "test-category-no-id-14").asRight)
    assertEquals(decode(JsonStringWithoutId15)(categoryDecoder), Category(BigInt(15), "test-category-no-id-15").asRight)
    assertEquals(nextId, BigInt(16))

    // Previous category JSON strings but without IDs.
    assertEquals(decode(jsonStringWithoutId10)(categoryDecoder), Category(BigInt(10), "test-category-10").asRight)
    assertEquals(decode(jsonStringWithoutId11)(categoryDecoder), Category(BigInt(11), "test-category-11").asRight)
    assertEquals(decode(jsonStringWithoutId12)(categoryDecoder), Category(BigInt(12), "test-category-12").asRight)
    assertEquals(nextId, BigInt(16))
  }

  lazy val categoryFixture: FunFixture[Category] = FunFixture[Category](
    setup = { testOptions => Category(0, s"test-category (${testOptions.name})") },
    teardown = _ => resetId(),
  )

  lazy val jsonFixture: FunFixture[Json] = FunFixture[Json](
    setup = { testOptions => Json.obj(("id", 0.asJson), ("name", s"test-category (${testOptions.name})".asJson)) },
    teardown = _ => resetId(),
  )

  lazy val jsonStringFixture: FunFixture[String] = FunFixture[String](
    setup = { testOptions => s"""{ "id": 0, "name": "test-category (${testOptions.name})" }""" },
    teardown = _ => resetId(),
  )
}
