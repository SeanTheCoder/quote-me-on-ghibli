package io.seanthecoder.qmog
package codecs

import codecs.HealthCodec.{healthDecoder, healthEncoder, healthEntityDecoder, healthEntityEncoder}
import models.Health

import cats.MonadThrow
import cats.effect.IO
import cats.implicits.catsSyntaxEitherId
import io.circe.Json
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import munit.CatsEffectSuite
import org.http4s.Response

class HealthCodecSpec extends CatsEffectSuite {
  FunFixture.map2(healthFixture, jsonFixture).test(name = "healthEncoder should encode object to JSON") {
    case (health, json) => assertEquals(health.asJson(healthEncoder), json)
  }

  FunFixture.map2(jsonStringFixture, healthFixture).test(name = "healthDecoder should decode string to object") {
    case (jsonString, health) => assertEquals(decode(jsonString)(healthDecoder), health.asRight)
  }

  healthFixture.test(name = "healthEntityEncoder should encode to what healthEntityDecoder can decode") { health =>
    val response = Response[IO]().withEntity(health)(healthEntityEncoder)
    val resBody  = response.as[Health](MonadThrow[IO], healthEntityDecoder)
    assertIO(resBody, health)
  }

  lazy val healthFixture: FunFixture[Health] = FunFixture[Health](
    setup = { testOptions => Health(testOptions.name) },
    teardown = _ => (),
  )

  lazy val jsonFixture: FunFixture[Json] = FunFixture[Json](
    setup = { testOptions => Json.obj(("health", testOptions.name.asJson)) },
    teardown = _ => (),
  )

  lazy val jsonStringFixture: FunFixture[String] = FunFixture[String](
    setup = { testOptions => s"""{ "health": "${testOptions.name}" }""" },
    teardown = _ => (),
  )
}
