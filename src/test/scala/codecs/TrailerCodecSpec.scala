package io.seanthecoder.qmog
package codecs

import codecs.TrailerCodec.{trailerDecoder, trailerEncoder}
import models.Trailer

import cats.implicits.catsSyntaxEitherId
import io.circe.Json
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import munit.FunSuite

class TrailerCodecSpec extends FunSuite {
  FunFixture.map2(trailerFixture, jsonFixture).test(name = "trailerEncoder should encode object to JSON") {
    case (trailer, json) => assertEquals(trailer.asJson(trailerEncoder), json)
  }

  FunFixture.map2(jsonStringFixture, trailerFixture).test(name = "trailerDecoder should decode string to object") {
    case (jsonString, trailer) => assertEquals(decode(jsonString)(trailerDecoder), trailer.asRight)
  }

  lazy val trailerFixture: FunFixture[Trailer] = FunFixture[Trailer](
    setup = { testOptions => Trailer(s"test-youtube-video-id (${testOptions.name})") },
    teardown = _ => (),
  )

  lazy val jsonFixture: FunFixture[Json] = FunFixture[Json](
    setup = { testOptions => Json.obj(("youtubeVideoId", s"test-youtube-video-id (${testOptions.name})".asJson)) },
    teardown = _ => (),
  )

  lazy val jsonStringFixture: FunFixture[String] = FunFixture[String](
    setup = { testOptions => s"""{ "youtubeVideoId": "test-youtube-video-id (${testOptions.name})" }""" },
    teardown = _ => (),
  )
}
