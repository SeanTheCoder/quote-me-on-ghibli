package io.seanthecoder.qmog
package codecs

import codecs.InfoCodec.{infoDecoder, infoEncoder, infoEntityDecoder, infoEntityEncoder}
import models.Info

import buildInfo.BuildInfo
import cats.MonadThrow
import cats.effect.IO
import cats.implicits.{catsSyntaxEitherId, toShow}
import io.circe.Json
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import munit.CatsEffectSuite
import org.http4s.Response

class InfoCodecSpec extends CatsEffectSuite {
  FunFixture.map2(infoFixture, jsonFixture).test(name = "infoEncoder should encode object to JSON") {
    case (info, json) => assertEquals(info.asJson(infoEncoder), json)
  }

  FunFixture.map2(jsonStringFixture, infoFixture).test(name = "infoDecoder should decode string to object") {
    case (jsonString, info) => assertEquals(decode(jsonString)(infoDecoder), info.asRight)
  }

  infoFixture.test(name = "infoEntityEncoder should encode to what infoEntityDecoder can decode") { info =>
    val response = Response[IO]().withEntity(info)(infoEntityEncoder)
    val resBody  = response.as[Info](MonadThrow[IO], infoEntityDecoder)
    assertIO(resBody, info)
  }

  lazy val infoFixture: FunFixture[Info] = FunFixture[Info](
    setup = { testOptions =>
      Info(
        name = testOptions.name,
        description = "test-description",
        version = "0.0.0",
        scalaVersion = "2.3.7",
        sbtVersion = "1.5.5",
        builtAt = BuildInfo.builtAtString,
        builtAtMillis = BuildInfo.builtAtMillis,
        sourceVersion = BuildInfo.sourceVersion,
      )
    },
    teardown = _ => (),
  )

  lazy val jsonFixture: FunFixture[Json] = FunFixture[Json](
    setup = { testOptions =>
      val fields = List(
        "name"          -> testOptions.name.asJson,
        "description"   -> "test-description".asJson,
        "version"       -> "0.0.0".asJson,
        "scalaVersion"  -> "2.3.7".asJson,
        "sbtVersion"    -> "1.5.5".asJson,
        "builtAt"       -> BuildInfo.builtAtString.asJson,
        "builtAtMillis" -> BuildInfo.builtAtMillis.asJson,
        "sourceVersion" -> BuildInfo.sourceVersion.fold(Json.Null)(_.asJson),
      )
      Json.obj(fields: _*)
    },
    teardown = _ => (),
  )

  lazy val jsonStringFixture: FunFixture[String] = FunFixture[String](
    setup = { testOptions =>
      s"""
         |{
         |  "name": "${testOptions.name}",
         |  "description": "test-description",
         |  "version": "0.0.0",
         |  "scalaVersion": "2.3.7",
         |  "sbtVersion": "1.5.5",
         |  "builtAt": "${BuildInfo.builtAtString}",
         |  "builtAtMillis": "${BuildInfo.builtAtMillis.show}",
         |  "sourceVersion": ${BuildInfo.sourceVersion.fold("null")(ver => s"\"$ver\"")}
         |}
         |""".stripMargin
    },
    teardown = _ => (),
  )
}
