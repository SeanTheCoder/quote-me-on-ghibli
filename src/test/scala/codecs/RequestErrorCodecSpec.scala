package io.seanthecoder.qmog
package codecs

import codecs.ErrorBodyCodec.{errorBodyDecoder, errorBodyEncoder, errorBodyEntityDecoder, errorBodyEntityEncoder}
import models.RequestError

import cats.MonadThrow
import cats.effect.IO
import cats.implicits.catsSyntaxEitherId
import io.circe.Json
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import munit.CatsEffectSuite
import org.http4s.Response

class RequestErrorCodecSpec extends CatsEffectSuite {
  FunFixture.map2(errorBodyFixture, jsonFixture).test(name = "errorBodyEncoder should encode object to JSON") {
    case (health, json) => assertEquals(health.asJson(errorBodyEncoder), json)
  }

  FunFixture.map2(jsonStringFixture, errorBodyFixture).test(name = "errorBodyDecoder should decode string to object") {
    case (jsonString, errorBody) => assertEquals(decode(jsonString)(errorBodyDecoder), errorBody.asRight)
  }

  errorBodyFixture.test(name = "errorBodyEntityEncoder should encode to what errorBodyDecoder can decode") { errorBody =>
    val response = Response[IO]().withEntity(errorBody)(errorBodyEntityEncoder)
    val resBody  = response.as[RequestError](MonadThrow[IO], errorBodyEntityDecoder)
    assertIO(resBody, errorBody)
  }

  lazy val errorBodyFixture: FunFixture[RequestError] = FunFixture[RequestError](
    setup = { testOptions => RequestError(testOptions.name) },
    teardown = _ => (),
  )

  lazy val jsonFixture: FunFixture[Json] = FunFixture[Json](
    setup = { testOptions => Json.obj(("message", testOptions.name.asJson)) },
    teardown = _ => (),
  )

  lazy val jsonStringFixture: FunFixture[String] = FunFixture[String](
    setup = { testOptions => s"""{ "message": "${testOptions.name}" }""" },
    teardown = _ => (),
  )
}
