package io.seanthecoder.qmog
package codecs

import models.{Category, Movie, Trailer}

import cats.MonadThrow
import cats.effect.IO
import cats.implicits.catsSyntaxEitherId
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import io.circe.{DecodingFailure, Json}
import munit.CatsEffectSuite
import org.http4s.Response

import java.time.LocalDate
import scala.concurrent.duration.{DurationInt, FiniteDuration}

class MovieCodecSpec extends CatsEffectSuite {
  private lazy val movieCodec = MovieCodec()

  import movieCodec.{durationDecoder, durationEncoder, movieDecoder, movieEncoder, moviesEntityDecoder, moviesEntityEncoder, resetId}

  test(name = "durationEncoder should encode object to JSON") {
    assertEquals(1.hour.asJson(durationEncoder), 60.asJson)
  }

  test(name = "durationDecoder should decode string to object") {
    assertEquals(60.asJson.as[FiniteDuration](durationDecoder), 1.hour.asRight)
  }

  FunFixture.map2(movieFixture, jsonFixture).test(name = "movieEncoder should encode object to JSON") {
    case (movie, json) => assertEquals(movie.asJson(movieEncoder), json)
  }

  FunFixture.map2(jsonStringFixture, movieFixture).test(name = "movieDecoder should decode JSON to object") {
    case (jsonString, movie) =>
      val input = jsonString(LocalDate.now.getYear, "1-nausicaa.jpg", "1-nausicaa.jpg")
      assertEquals(decode(input)(movieDecoder), movie.asRight)
  }

  jsonStringFixture.test(name = "parse should fail when release year is negative") { jsonString =>
    val negativeYear = 0 - LocalDate.now.getYear
    val input        = jsonString(negativeYear, "1-nausicaa.jpg", "1-nausicaa.jpg")
    val failure      = DecodingFailure(s"Negative release year: $negativeYear", Nil)
    assertEquals(decode(input)(movieDecoder), failure.asLeft)
  }

  jsonStringFixture.test(name = "parse should fail when profile image does not exist") { jsonString =>
    val nonExistent = "non-existent-image.jpeg"
    val input       = jsonString(LocalDate.now.getYear, nonExistent, "1-nausicaa.jpg")
    val failure     = DecodingFailure(s"Image not found in subfolder \"profiles\": $nonExistent", Nil)
    assertEquals(decode(input)(movieDecoder), failure.asLeft)
  }

  jsonStringFixture.test(name = "parse should fail when poster image does not exist") { jsonString =>
    val nonExistent = "non-existent-image.jpeg"
    val input       = jsonString(LocalDate.now.getYear, "1-nausicaa.jpg", nonExistent)
    val failure     = DecodingFailure(s"Image not found in subfolder \"posters\": $nonExistent", Nil)
    assertEquals(decode(input)(movieDecoder), failure.asLeft)
  }

  movieFixture.test(name = "moviesEntityEncoder should encode to what moviesEntityDecoder can decode") { movie =>
    val movies   = List(movie, movie.copy(id = BigInt(2)), movie.copy(id = BigInt(3)))
    val response = Response[IO]().withEntity(movies)(moviesEntityEncoder)
    val resBody  = response.as[List[Movie]](MonadThrow[IO], moviesEntityDecoder)
    assertIO(resBody, movies)
  }

  lazy val movieFixture: FunFixture[Movie] = FunFixture[Movie](
    setup = { testOptions =>
      Movie(
        id = 1,
        rank = 1,
        title = "test-title",
        summary = "test-summary",
        description = s"test-description (${testOptions.name})",
        runningTime = 2.hours,
        director = "test-director",
        releaseYear = LocalDate.now.getYear,
        profileImage = "/image/profile/1-nausicaa.jpg",
        posterImage = "/image/poster/1-nausicaa.jpg",
        trailer = Trailer("test-youtube-video-id"),
        categories = List(Category(id = 0, "test-category")),
      )
    },
    teardown = _ => (),
  )

  lazy val jsonFixture: FunFixture[Json] = FunFixture[Json](
    setup = {
      testOptions =>
        val fields = List(
          "id"           -> 1.asJson,
          "rank"         -> 1.asJson,
          "title"        -> "test-title".asJson,
          "summary"      -> "test-summary".asJson,
          "description"  -> s"test-description (${testOptions.name})".asJson,
          "runningTime"  -> 2.hours.asJson,
          "director"     -> "test-director".asJson,
          "releaseYear"  -> LocalDate.now.getYear.asJson,
          "profileImage" -> "/image/profile/1-nausicaa.jpg".asJson,
          "posterImage"  -> "/image/poster/1-nausicaa.jpg".asJson,
          "trailer"      -> Json.obj(("youtubeVideoId", "test-youtube-video-id".asJson)),
          "categories"   -> Json.arr(Json.obj(("id", 0.asJson), ("name", "test-category".asJson))),
        )
        Json.obj(fields: _*)
    },
    teardown = _ => (),
  )

  lazy val jsonStringFixture: FunFixture[(Int, String, String) => String] = FunFixture[(Int, String, String) => String](
    setup = { testOptions => (year: Int, profile: String, poster: String) =>
      s"""
          |{
          |  "id": 1,
          |  "rank": 1,
          |  "title": "test-title",
          |  "summary": "test-summary",
          |  "description": "test-description (${testOptions.name})",
          |  "runningTime": 120,
          |  "director": "test-director",
          |  "releaseYear": $year,
          |  "profileImage": "$profile",
          |  "posterImage": "$poster",
          |  "trailer": {
          |    "youtubeVideoId": "test-youtube-video-id"
          |  },
          |  "categories": [
          |    {
          |      "id": 0,
          |      "name": "test-category"
          |    }
          |  ]
          |}
          |""".stripMargin
    },
    teardown = _ => (),
  )

  override def afterEach(context: AfterEach): Unit = resetId()
}
