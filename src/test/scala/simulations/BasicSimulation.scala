package io.seanthecoder.qmog
package simulations

import buildInfo.BuildInfo
import io.gatling.core.Predef._
import io.gatling.core.structure.{ChainBuilder, ScenarioBuilder}
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import io.seanthecoder.qmog.simulations.BasicSimulation.targetUrl

import scala.concurrent.duration._

class BasicSimulation extends Simulation {
  val httpProtocol: HttpProtocolBuilder = http
    .warmUp(targetUrl)
    .baseUrl(targetUrl)
    .acceptHeader(value = "application/json")
    .acceptEncodingHeader(value = "gzip, deflate, br")
    .userAgentHeader(value = s"Gatling/${BuildInfo.gatlingVersion}")

  val getInfo: ChainBuilder = repeat(times = 2) {
    exec(http(requestName = "Get Info")
      .get("/").check(status.in(expected = 200 to 299), jsonPath(path = "$.name").is(expected = "quote-me-on-ghibli")))
  }

  val getCategories: ChainBuilder = exec(http(requestName = "Get Categories").get("/category/all"))

  val getMovies: ChainBuilder = exec(http(requestName = "Get Movies")
    .get("/movie/all").check(jsonPath(path = "$[10].id").saveAs(key = "movieId")))

  val scn: ScenarioBuilder = scenario(name = "Basic Scenario")
    .exec(getInfo).pause(900.millis)
    .exec(getCategories).pause(900.millis)
    .exec(getMovies).pause(900.millis)
    .exec { session => println(s"Movie ID: ${session("movieId").as[String]}"); session }
    .pause(900.millis)
    .exec(http(requestName = "Get Movie")
      .get("/movie/#{movieId}").check(jsonPath(path = "$.id").is(expected = "#{movieId}")))
    .pause(900.millis)
    .exec(http(requestName = "Query By Keyword")
      .get("/movie/search")
      .queryParam(name = "keyword", value = "japan"))
    .pause(900.millis)
    .exec(http(requestName = "Query By Category ID")
      .get("/movie/search")
      .queryParam(name = "categoryId", value = "0"))
    .pause(900.millis)
    .exec(http(requestName = "Query By Category Name")
      .get("/movie/search")
      .queryParam(name = "categoryName", value = "Romance"))
    .pause(900.millis)

  setUp(
    scn.inject(
      stressPeakUsers(users = 30).during(3.seconds),
    ).protocols(httpProtocol.inferHtmlResources()),
  ).maxDuration(10.minutes)
}

object BasicSimulation {
  val targetUrl: String = "https://quote-me-on-ghibli.herokuapp.com"
}
