package io.seanthecoder.qmog

import com.comcast.ip4s.IpLiteralSyntax
import munit.FunSuite

class UtilsSpec extends FunSuite {
  test(name = "serverPort should return port from envMap") {
    val envMap = Map("PORT" -> "8765")
    assertEquals(Utils.serverPort(envMap), expected = port"8765")
  }

  test(name = "serverPort should return default port for empty envMap") {
    val envMap: Map[String, String] = Map.empty
    assertEquals(Utils.serverPort(envMap), expected = port"8080")
  }

  test(name = "baseUrl should return Heroku app URL from envMap") {
    val envMap = Map("HEROKU_APP_NAME" -> "test-app")
    assertEquals(Utils.baseUrl(envMap), expected = "https://test-app.herokuapp.com")
  }

  test(name = "baseUrl should return empty string for empty envMap") {
    val envMap: Map[String, String] = Map.empty
    assertEquals(Utils.baseUrl(envMap), expected = "")
  }

  test(name = "pluralise and singularise should convert known words") {
    val poster   = "poster"
    val posters  = "posters"
    val profile  = "profile"
    val profiles = "profiles"
    val unknown  = "unknown"

    assertEquals(Utils.pluralise(poster), expected = posters)
    assertEquals(Utils.pluralise(profile), expected = profiles)
    assertEquals(Utils.pluralise(unknown), expected = unknown)

    assertEquals(Utils.singularise(posters), expected = poster)
    assertEquals(Utils.singularise(profiles), expected = profile)
    assertEquals(Utils.singularise(unknown), expected = unknown)
  }
}
