package io.seanthecoder.qmog
package fixtures

import models.Info

import buildInfo.BuildInfo
import io.circe.Json
import io.circe.syntax.EncoderOps
import org.http4s.headers.Location
import org.http4s.implicits.http4sLiteralsSyntax

object InfoFixture {
  val infoLocationHeader: Location = Location(uri"/info")

  val info: Info = Info(
    name = "quote-me-on-ghibli",
    description = "Quote Me on Ghibli",
    version = "0.1",
    scalaVersion = "2.13.7",
    sbtVersion = "1.5.7",
    builtAt = BuildInfo.builtAtString,
    builtAtMillis = BuildInfo.builtAtMillis,
    sourceVersion = BuildInfo.sourceVersion,
  )

  val infoJson: Json = Json.obj(
    ("name", "quote-me-on-ghibli".asJson),
    ("description", "Quote Me on Ghibli".asJson),
    ("version", "0.1".asJson),
    ("scalaVersion", "2.13.8".asJson),
    ("sbtVersion", "1.6.2".asJson),
    ("builtAt", BuildInfo.builtAtString.asJson),
    ("builtAtMillis", BuildInfo.builtAtMillis.asJson),
    ("sourceVersion", BuildInfo.sourceVersion.asJson),
  )
}
