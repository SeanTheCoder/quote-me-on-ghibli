package io.seanthecoder.qmog
package fixtures

import models.{Category, Movie, Trailer}

import scala.concurrent.duration.DurationInt

object MovieFixture {
  val movies: List[Movie] = List(
    Movie(
      BigInt(0),
      10L,
      "test-title-1",
      "test-summary-1",
      "test-description-1",
      110.minutes,
      "test-director-1",
      2001,
      "/image/profile/1-nausicaa.jpg",
      "/image/poster/1-nausicaa.jpg",
      Trailer("youtube-video-id-1"),
      categories = List(
        Category(BigInt(0), "test-category-0"),
        Category(BigInt(1), "test-category-1"),
      ),
    ),
    Movie(
      BigInt(1),
      20L,
      "test-title-2",
      "test-summary-2",
      "test-description-2",
      120.minutes,
      "test-director-2",
      2002,
      "/image/profile/2-laputa.jpg",
      "/image/poster/2-laputa.jpg",
      Trailer("youtube-video-id-2"),
      categories = List(
        Category(BigInt(1), "test-category-1"),
        Category(BigInt(2), "test-category-2"),
      ),
    ),
    Movie(
      BigInt(2),
      30L,
      "test-title-3",
      "test-summary-3",
      "test-description-3",
      130.minutes,
      "test-director-3",
      2003,
      "/image/profile/3-totoro.jpg",
      "/image/poster/3-totoro.jpg",
      Trailer("youtube-video-id-3"),
      categories = List(
        Category(BigInt(3), "test-category-3"),
        Category(BigInt(4), "test-category-4"),
        Category(BigInt(5), "test-category-5"),
      ),
    ),
  )

  val moviesWithCategory1: List[Movie] = movies.take(2)
}
