package io.seanthecoder.qmog
package fixtures

import models.Health

import io.circe.Json
import io.circe.syntax.EncoderOps

object HealthFixture {
  val health: Health   = Health("ok")
  val healthJson: Json = Json.obj(("health", "ok".asJson))
}
