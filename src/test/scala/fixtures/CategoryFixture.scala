package io.seanthecoder.qmog
package fixtures

import models.Category

object CategoryFixture {
  val categories: List[Category] = List(
    Category(BigInt(0), "test-category-0"),
    Category(BigInt(1), "test-category-1"),
    Category(BigInt(2), "test-category-2"),
    Category(BigInt(3), "test-category-3"),
    Category(BigInt(4), "test-category-4"),
    Category(BigInt(5), "test-category-5"),
  )
}
