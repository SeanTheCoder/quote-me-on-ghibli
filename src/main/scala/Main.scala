package io.seanthecoder.qmog

import http.Server

import cats.effect.{ExitCode, IO, IOApp}

object Main extends IOApp {
  override def run(args: List[String]): IO[ExitCode] =
    Server[IO].stream.compile.drain.as(ExitCode.Success)
}
