package io.seanthecoder.qmog
package models

import scala.concurrent.duration.FiniteDuration

case class Movie(
  id: BigInt,
  rank: Long,
  title: String,
  summary: String,
  description: String,
  runningTime: FiniteDuration,
  director: String,
  releaseYear: Int,
  profileImage: String,
  posterImage: String,
  trailer: Trailer,
  categories: List[Category],
)

object Movie {
  implicit val movieOrdering: Ordering[Movie] =
    (prev: Movie, next: Movie) => prev.id.compareTo(next.id)
}
