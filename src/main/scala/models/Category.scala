package io.seanthecoder.qmog
package models

case class Category(id: BigInt, name: String)

object Category {
  implicit val categoryOrdering: Ordering[Category] =
    (prev: Category, next: Category) => prev.id.compareTo(next.id)
}
