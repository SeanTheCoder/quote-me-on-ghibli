package io.seanthecoder.qmog
package models

case class RequestError(message: String)

object RequestError {
  def fromThrowable(summary: String, throwable: Throwable): RequestError =
    RequestError(s"$summary - ${throwable.getMessage}")
}
