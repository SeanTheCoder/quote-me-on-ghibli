package io.seanthecoder.qmog
package models

case class Info(
  name: String,
  description: String,
  version: String,
  scalaVersion: String,
  sbtVersion: String,
  builtAt: String,
  builtAtMillis: Long,
  sourceVersion: Option[String],
)
