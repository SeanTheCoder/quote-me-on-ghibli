package io.seanthecoder.qmog
package codecs

import Utils.{baseUrl, pluralise, singularise}
import codecs.TrailerCodec.{trailerDecoder, trailerEncoder}
import models.{Category, Movie, Trailer}

import cats.effect.Concurrent
import cats.implicits.{catsSyntaxEitherId, toShow}
import io.circe.Decoder.Result
import io.circe.syntax.EncoderOps
import io.circe.{Decoder, DecodingFailure, Encoder, HCursor, Json}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

import java.net.URL
import java.nio.file.Paths
import scala.concurrent.duration.{Duration, DurationLong, FiniteDuration}
import scala.util.Try

class MovieCodec {
  val categoryCodec: CategoryCodec = CategoryCodec()
  import categoryCodec.{categoryDecoder, categoryEncoder}

  implicit val durationEncoder: Encoder[FiniteDuration] =
    (duration: Duration) => duration.toMinutes.asJson

  implicit val durationDecoder: Decoder[FiniteDuration] =
    (cursor: HCursor) => cursor.as[Long].map(_.minutes)

  implicit val movieEncoder: Encoder[Movie] = (movie: Movie) => {
    val fieldMap = List(
      "id"           -> movie.id.asJson,
      "rank"         -> movie.rank.asJson,
      "title"        -> movie.title.asJson,
      "summary"      -> movie.summary.asJson,
      "description"  -> movie.description.asJson,
      "runningTime"  -> movie.runningTime.asJson,
      "director"     -> movie.director.asJson,
      "releaseYear"  -> movie.releaseYear.asJson,
      "profileImage" -> movie.profileImage.asJson,
      "posterImage"  -> movie.posterImage.asJson,
      "trailer"      -> movie.trailer.asJson,
      "categories"   -> movie.categories.asJson,
    )
    Json.obj(fieldMap: _*)
  }

  implicit val movieDecoder: Decoder[Movie] = (cursor: HCursor) => {
    val validateReleaseYear = (year: Int) =>
      if (year >= 0) year.asRight
      else DecodingFailure(s"Negative release year: ${year.show}", cursor.history).asLeft

    def validateImageName(subfolder: String)(imageName: String): Result[String] = {
      val classLoader = getClass.getClassLoader

      val fileName = Try(Paths.get(imageName)).fold(
        DecodingFailure.fromThrowable(_, cursor.history).asLeft[String],
        _.getFileName.toString.asRight[DecodingFailure],
      )

      val resourceUrl = fileName.map(name => Option(classLoader.getResource(s"assets/${pluralise(subfolder)}/$name")))
        .flatMap {
          val failure = DecodingFailure(s"Image not found in subfolder \"$subfolder\": $imageName", cursor.history)
          _.fold(failure.asLeft[URL])(_.asRight[DecodingFailure])
        }

      resourceUrl.map(_ => s"${baseUrl(sys.env)}/image/${singularise(subfolder)}/${Paths.get(imageName).getFileName.toString}")
    }

    for {
      id           <- cursor.get[BigInt]("id")
      rank         <- cursor.get[Long]("rank")
      title        <- cursor.get[String]("title")
      summary      <- cursor.get[String]("summary")
      description  <- cursor.get[String]("description")
      runningTime  <- cursor.get[FiniteDuration]("runningTime")
      director     <- cursor.get[String]("director")
      releaseYear  <- cursor.get[Int]("releaseYear").flatMap(validateReleaseYear)
      profileImage <- cursor.get[String]("profileImage").flatMap(validateImageName(subfolder = "profiles"))
      posterImage  <- cursor.get[String]("posterImage").flatMap(validateImageName(subfolder = "posters"))
      trailer      <- cursor.get[Trailer]("trailer")
      categories   <- cursor.get[List[Category]]("categories")
    } yield Movie(
      id,
      rank,
      title,
      summary,
      description,
      runningTime,
      director,
      releaseYear,
      profileImage,
      posterImage,
      trailer,
      categories,
    )
  }

  implicit def movieEntityEncoder[F[_]]: EntityEncoder[F, Movie] =
    jsonEncoderOf[F, Movie]

  implicit def movieEntityDecoder[F[_]: Concurrent]: EntityDecoder[F, Movie] =
    jsonOf[F, Movie]

  implicit def moviesEntityEncoder[F[_]]: EntityEncoder[F, List[Movie]] =
    jsonEncoderOf[F, List[Movie]]

  implicit def moviesEntityDecoder[F[_]: Concurrent]: EntityDecoder[F, List[Movie]] =
    jsonOf[F, List[Movie]]

  // Expose the following for ease of testing.
  def resetId(): Unit = categoryCodec.resetId()
}

object MovieCodec {
  def apply(): MovieCodec = new MovieCodec()
}
