package io.seanthecoder.qmog
package codecs

import models.RequestError

import cats.effect.Concurrent
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

object ErrorBodyCodec {
  implicit val errorBodyEncoder: Encoder[RequestError] = deriveEncoder[RequestError]
  implicit val errorBodyDecoder: Decoder[RequestError] = deriveDecoder[RequestError]

  implicit def errorBodyEntityEncoder[F[_]]: EntityEncoder[F, RequestError] =
    jsonEncoderOf[F, RequestError]

  implicit def errorBodyEntityDecoder[F[_]: Concurrent]: EntityDecoder[F, RequestError] =
    jsonOf[F, RequestError]
}
