package io.seanthecoder.qmog
package codecs

import models.Trailer

import io.circe.{Decoder, Encoder}

object TrailerCodec {
  implicit val trailerEncoder: Encoder[Trailer] =
    Encoder.forProduct1(nameA0 = "youtubeVideoId")(_.youtubeVideoId)

  implicit val trailerDecoder: Decoder[Trailer] =
    Decoder.forProduct1(nameA0 = "youtubeVideoId")(Trailer)
}
