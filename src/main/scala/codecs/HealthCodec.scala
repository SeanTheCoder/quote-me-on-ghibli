package io.seanthecoder.qmog
package codecs

import models.Health

import cats.effect.Concurrent
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

object HealthCodec {
  implicit val healthEncoder: Encoder[Health] = deriveEncoder[Health]
  implicit val healthDecoder: Decoder[Health] = deriveDecoder[Health]

  implicit def healthEntityEncoder[F[_]]: EntityEncoder[F, Health] =
    jsonEncoderOf[F, Health]

  implicit def healthEntityDecoder[F[_]: Concurrent]: EntityDecoder[F, Health] =
    jsonOf[F, Health]
}
