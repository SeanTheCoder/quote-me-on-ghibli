package io.seanthecoder.qmog
package codecs

import models.Info

import cats.effect.Concurrent
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

object InfoCodec {
  implicit val infoEncoder: Encoder[Info] = deriveEncoder[Info]
  implicit val infoDecoder: Decoder[Info] = deriveDecoder[Info]

  implicit def infoEntityEncoder[F[_]]: EntityEncoder[F, Info] =
    jsonEncoderOf[F, Info]

  implicit def infoEntityDecoder[F[_]: Concurrent]: EntityDecoder[F, Info] =
    jsonOf[F, Info]
}
