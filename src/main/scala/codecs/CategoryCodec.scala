package io.seanthecoder.qmog
package codecs

import models.Category

import cats.effect.Concurrent
import cats.implicits.catsSyntaxOptionId
import io.circe.Decoder.Result
import io.circe.{Decoder, DecodingFailure, Encoder, HCursor}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

import java.util.concurrent.atomic.AtomicReference
import scala.collection.immutable.TreeMap
import scala.math.BigInt.int2bigInt

class CategoryCodec {
  implicit val categoryEncoder: Encoder[Category] =
    Encoder.forProduct2(nameA0 = "id", nameA1 = "name")(category => (category.id, category.name))

  implicit val categoryDecoder: Decoder[Category] = (cursor: HCursor) =>
    for {
      optionId         <- cursor.downField("id").as[Option[BigInt]]
      optionName       <- cursor.downField("name").as[Option[String]]
      providedOrAutoId <- getCategoryId(optionId, optionName, cursor)
      (id, name)       <- registerCategory(providedOrAutoId, optionName, cursor)
    } yield Category(id, name)

  implicit def categoryEntityEncoder[F[_]]: EntityEncoder[F, Category] =
    jsonEncoderOf[F, Category]

  implicit def categoryEntityDecoder[F[_]: Concurrent]: EntityDecoder[F, Category] =
    jsonOf[F, Category]

  implicit def categoriesEntityEncoder[F[_]]: EntityEncoder[F, List[Category]] =
    jsonEncoderOf[F, List[Category]]

  implicit def categoriesEntityDecoder[F[_]: Concurrent]: EntityDecoder[F, List[Category]] =
    jsonOf[F, List[Category]]

  // Expose the following for ease of testing.
  def nextId: BigInt  = idAutoIncrementer.get()
  def resetId(): Unit = idAutoIncrementer.set(0)

  private val reverseIndex: AtomicReference[Map[String, BigInt]] = new AtomicReference(TreeMap.empty)
  private val idAutoIncrementer: AtomicReference[BigInt]         = new AtomicReference(0)

  private def nextIdAndIncrement: BigInt = idAutoIncrementer.getAndAccumulate(1, _ + _)

  private def rebaseIdAutoIncrementer(id: BigInt): BigInt = {
    val _ = idAutoIncrementer.accumulateAndGet(id, (curId, newId) => if (curId <= newId) newId + 1 else curId)
    id
  }

  private def getCategoryId(optionId: Option[BigInt], optionName: Option[String], cursor: HCursor): Result[BigInt] = {
    val providedOrAutoId = for {
      name <- optionName
      id   <- optionId
                .map(rebaseIdAutoIncrementer)
                .orElse(reverseIndex.get().getOrElse(name, nextIdAndIncrement).some)
    } yield id
    providedOrAutoId.toRight(DecodingFailure("Unexpected empty ID", cursor.history))
  }

  private def registerCategory(id: BigInt, optionName: Option[String], cursor: HCursor): Result[(BigInt, String)] =
    optionName.map { name =>
      val _ = reverseIndex.accumulateAndGet(Map(name -> id), _ ++ _)
      (id, name)
    }.toRight(DecodingFailure("Unexpected empty name", cursor.history))
}

object CategoryCodec {
  def apply(): CategoryCodec = new CategoryCodec()
}
