package io.seanthecoder.qmog
package services

import codecs.MovieCodec
import models.{Category, Movie, RequestError}

import cats.effect.Async
import cats.effect.implicits.genConcurrentOps_
import cats.implicits.{
  catsSyntaxApplicativeErrorId,
  catsSyntaxApplicativeId,
  catsSyntaxEitherId,
  catsSyntaxNestedId,
  catsSyntaxOptionId,
  none,
  toFlatMapOps,
  toFunctorOps,
  toShow,
}
import io.circe.Decoder.decodeList
import io.circe.jawn.decode

import scala.io.Codec.UTF8
import scala.io.Source
import scala.util.Using.Releasable.AutoCloseableIsReleasable
import scala.util.{Try, Using}

trait MovieCollection[F[_]] {
  val movieCodec: MovieCodec

  def getMovies: F[List[Movie]]
  def getCategories: F[List[Category]]
  def findById(id: String): F[Either[RequestError, Option[Movie]]]

  def search(
    catId: Option[String],
    catName: Option[String],
    keyword: Option[String],
  ): F[Either[RequestError, List[Movie]]]

  // Expose the following for ease of testing.
  def resetId(): Unit
}

object MovieCollection {
  val moviesFileName = "movies.prod.json"

  def apply[F[_]: Async]: MovieCollection[F] = apply[F](moviesFileName)

  def apply[F[_]: Async](fileName: String): MovieCollection[F] = new MovieCollection[F] {
    override val movieCodec: MovieCodec = MovieCodec()
    import movieCodec.movieDecoder

    override def getMovies: F[List[Movie]] = memoisedMovies.flatten

    override def getCategories: F[List[Category]] = memoisedCategories.flatten

    override def findById(movieId: String): F[Either[RequestError, Option[Movie]]] =
      for {
        id    <- Try(BigInt(movieId)).fold(
                   throwable => RequestError.fromThrowable(summary = "Invalid Movie ID", throwable).asLeft[BigInt].pure[F],
                   _.asRight[RequestError].pure[F],
                 )
        movie <- id.fold(err => err.asLeft[Option[Movie]].pure[F], findByMovieId(_).map(_.asRight[RequestError]))
      } yield movie

    override def search(
      categoryId: Option[String],
      categoryName: Option[String],
      keyword: Option[String],
    ): F[Either[RequestError, List[Movie]]] =
      for {
        byCatIdEither       <- findByCategoryId(categoryId)
        byCatNameEither     <- findByCategoryName(categoryName)
        byTitleEither       <- findByTitle(keyword)
        bySummaryEither     <- findBySummary(keyword)
        byDescriptionEither <- findByDescription(keyword)
        byDirectorEither    <- findByDirector(keyword)
        movies               = for {
          byCatId       <- byCatIdEither
          byCatName     <- byCatNameEither
          byTitle       <- byTitleEither
          bySummary     <- bySummaryEither
          byDescription <- byDescriptionEither
          byDirector    <- byDirectorEither
          concatenated   = byCatId ++ byCatName ++ byTitle ++ bySummary ++ byDescription ++ byDirector
          movies         = concatenated.distinct.sorted
        } yield movies
      } yield movies

    override def resetId(): Unit = movieCodec.resetId()

    private def findByCategoryId(categoryId: Option[String]): F[Either[RequestError, List[Movie]]] = {
      val catId      = categoryId.flatMap(id => if (id.isBlank) none else id.some)
      val errOrCatId = catId.fold(BigInt(Long.MinValue).asRight[RequestError])(catId =>
        Try(BigInt(catId)).fold(RequestError.fromThrowable(summary = "Invalid Category ID", _).asLeft[BigInt], _.asRight),
      )
      errOrCatId.fold(error => error.asLeft[List[Movie]].pure[F], findWithBigIntSelector(_, _.categories.map(_.id)))
    }

    private def findByCategoryName(categoryName: Option[String]) =
      findWithStringSelector(categoryName, _.categories.map(_.name), exactMatch = true)

    private def findByTitle(keyword: Option[String])       = findWithStringSelector(keyword, movie => List(movie.title))
    private def findBySummary(keyword: Option[String])     = findWithStringSelector(keyword, movie => List(movie.summary))
    private def findByDescription(keyword: Option[String]) = findWithStringSelector(keyword, movie => List(movie.description))
    private def findByDirector(keyword: Option[String])    = findWithStringSelector(keyword, movie => List(movie.director))

    private def findWithBigIntSelector(
      keyword: BigInt,
      selector: Movie => List[BigInt],
    ): F[Either[RequestError, List[Movie]]] =
      findWithStringSelector(keyword.show.some, selector(_).map(_.show), exactMatch = true)

    private def findWithStringSelector(
      keyword: Option[String],
      selector: Movie => List[String],
      exactMatch: Boolean = false,
    ): F[Either[RequestError, List[Movie]]] = {
      val needle = keyword.flatMap(word => if (word.isBlank) none else word.trim.toLowerCase.some)

      val foundByField = for {
        movies  <- getMovies
        filtered = needle.map(word =>
                     movies.filter {
                       selector(_).map(_.toLowerCase).exists { haystack =>
                         if (exactMatch) haystack == word else haystack.contains(word)
                       }
                     }.distinct.sorted,
                   )
      } yield filtered

      for {
        byField  <- foundByField
        shortlist = byField.fold(List.empty[Movie].asRight[RequestError])(_.asRight)
      } yield shortlist
    }

    private def findByMovieId(id: BigInt): F[Option[Movie]] =
      for {
        movies <- getMovies
        movie  <- movies.find(_.id == id).pure[F]
      } yield movie

    private val memoisedMovies: F[F[List[Movie]]] = {
      val classLoader = getClass.getClassLoader
      val resourceUrl = Option(classLoader.getResource(fileName))
      val fileContent = resourceUrl.fold(new Exception(s"Failed to load file: $fileName").raiseError[Try, String]) { url =>
        Using(Source.fromURL(url)(UTF8))(_.mkString)
      }
      fileContent.fold(
        _.raiseError[F, List[Movie]],
        jsonString => decode[List[Movie]](jsonString).fold(_.raiseError[F, List[Movie]], _.sorted.pure[F]),
      ).memoize
    }

    private val memoisedCategories: F[F[List[Category]]] =
      getMovies.nested.map(_.categories).value.map(_.flatten.distinct.sorted).memoize
  }
}
