package io.seanthecoder.qmog
package services

import models.Health

import cats.Applicative
import cats.implicits.catsSyntaxApplicativeId

trait HealthCheck[F[_]] {
  def check(): F[Health]
}

object HealthCheck {
  def apply[F[_]: Applicative]: HealthCheck[F] = () => Health("ok").pure[F]
}
