package io.seanthecoder.qmog
package services

import models.Info

import buildInfo.BuildInfo
import cats.Applicative
import cats.implicits.catsSyntaxApplicativeId

trait ServerInfo[F[_]] {
  def show(): F[Info]
}

object ServerInfo {
  def apply[F[_]: Applicative]: ServerInfo[F] = () =>
    Info(
      name = BuildInfo.name,
      description = BuildInfo.description,
      version = BuildInfo.version,
      scalaVersion = BuildInfo.scalaVersion,
      sbtVersion = BuildInfo.sbtVersion,
      builtAt = BuildInfo.builtAtString,
      builtAtMillis = BuildInfo.builtAtMillis,
      sourceVersion = BuildInfo.sourceVersion,
    ).pure[F]
}
