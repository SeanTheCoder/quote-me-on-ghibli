package io.seanthecoder.qmog

import cats.implicits.catsSyntaxOptionId
import com.comcast.ip4s.{IpLiteralSyntax, Ipv4Address, Port}

object Utils {
  def serverPort(envMap: Map[String, String]): Port = {
    val fromEnv = for {
      portStr <- envMap.get("PORT")
      fromStr <- Port.fromString(portStr)
    } yield fromStr
    fromEnv.fold(defaultPort)(identity)
  }

  def baseUrl(envMap: Map[String, String]): String = {
    val url = for {
      appName <- envMap.get("HEROKU_APP_NAME")
      url     <- s"https://$appName.herokuapp.com".some
    } yield url
    url.fold("")(identity)
  }

  def pluralise(word: String): String = dictionary.getOrElse(word, word)

  def singularise(word: String): String = reverseDict.getOrElse(word, word)

  private val dictionary: Map[String, String] = Map(
    "poster"  -> "posters",
    "profile" -> "profiles",
  )

  private val reverseDict: Map[String, String] = for {
    (singular, plural) <- dictionary
  } yield (plural, singular)

  val defaultPort: Port                 = port"8080"
  val hostAddress: Ipv4Address          = ipv4"0.0.0.0"
  val imageFileExtensions: List[String] = List(".jpg", ".jpeg", ".png", ".gif", ".tiff")
}
