package io.seanthecoder.qmog
package http

import org.http4s.dsl.io.OptionalQueryParamDecoderMatcher

object QueryParamMatchers {
  object CategoryId      extends OptionalQueryParamDecoderMatcher[String](name = "categoryId")
  object CategoryName    extends OptionalQueryParamDecoderMatcher[String](name = "categoryName")
  object UniversalSearch extends OptionalQueryParamDecoderMatcher[String](name = "keyword")
}
