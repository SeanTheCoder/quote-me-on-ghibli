package io.seanthecoder.qmog
package http

import Utils.{imageFileExtensions, pluralise, singularise}
import codecs.ErrorBodyCodec.errorBodyEncoder
import codecs.HealthCodec.healthEntityEncoder
import codecs.InfoCodec.infoEntityEncoder
import codecs.MovieCodec
import http.QueryParamMatchers.{CategoryId, CategoryName, UniversalSearch}
import models.RequestError
import services.{HealthCheck, MovieCollection, ServerInfo}

import cats.effect.Async
import cats.implicits.{toFlatMapOps, toFunctorOps}
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.Location
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s.{HttpRoutes, Request, Response, StaticFile}

class Routes[F[_]: Async] {
  private val dsl: Http4sDsl[F] = new Http4sDsl[F] {}
  import dsl._

  private val movieCodec = MovieCodec()
  import movieCodec.categoryCodec.categoriesEntityEncoder
  import movieCodec.{movieEntityEncoder, moviesEntityEncoder}

  def healthRoutes(healthCheck: HealthCheck[F]): HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root / "health" => for {
          health   <- healthCheck.check()
          response <- Ok(health)
        } yield response
    }

  def movieRoutes(movieCollection: MovieCollection[F]): HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root / "movie" / "all" => for {
          movies   <- movieCollection.getMovies
          response <- Ok(movies)
        } yield response

      case GET -> Root / "movie" / "search" :? CategoryId(catId) +& CategoryName(catName) +& UniversalSearch(keyword) => for {
          errOrMovies <- movieCollection.search(catId, catName, keyword)
          response    <- errOrMovies.fold(BadRequest(_), Ok(_))
        } yield response

      case GET -> Root / "movie" / movieId => for {
          errOrMovie <- movieCollection.findById(movieId)
          notFound    = NotFound(RequestError(s"Movie with ID $movieId not found"))
          response   <- errOrMovie.fold(BadRequest(_), _.fold(notFound)(Ok(_)))
        } yield response
    }

  def categoryRoutes(movieCollection: MovieCollection[F]): HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root / "category" / "all" => for {
          categories <- movieCollection.getCategories
          response   <- Ok(categories)
        } yield response
    }

  def imageAssetRoutes: HttpRoutes[F] =
    HttpRoutes.of[F] {
      case req @ GET -> Root / "image" / "profile" / fileName if imageFileExtensions.exists(fileName.endsWith) =>
        routeImageAsset(req, imageType = "profile", fileName)

      case req @ GET -> Root / "image" / "poster" / fileName if imageFileExtensions.exists(fileName.endsWith) =>
        routeImageAsset(req, imageType = "poster", fileName)
    }

  def infoRoutes(serverInfo: ServerInfo[F]): HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root / "info" => for {
          info     <- serverInfo.show()
          response <- Ok(info)
        } yield response

      case GET -> Root => TemporaryRedirect(Location(uri"/info"))
    }

  private def routeImageAsset(request: Request[F], imageType: String, fileName: String): F[Response[F]] = StaticFile
    .fromResource[F](name = s"/assets/${pluralise(imageType)}/$fileName", req = Some(request), preferGzipped = true)
    .getOrElseF[Response[F]](NotFound(RequestError(s"Movie ${singularise(imageType)} with name $fileName not found")))
}

object Routes {
  def apply[F[_]: Async]: Routes[F] = new Routes[F]
}
