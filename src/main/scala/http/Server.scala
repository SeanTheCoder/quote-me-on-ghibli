package io.seanthecoder.qmog
package http

import Utils.{hostAddress, serverPort}
import services.{HealthCheck, MovieCollection, ServerInfo}

import cats.effect.{Async, Resource}
import cats.implicits.toSemigroupKOps
import cats.syntax.all.catsSyntaxFlatMapOps
import fs2.{Pure, Stream}
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.middleware.{CORS, Logger}
import org.http4s.{Http, HttpApp, HttpRoutes}

trait Server[F[_]] {
  def stream: Stream[F, Nothing]
}

object Server {
  def apply[F[_]: Async]: Server[F] = new Server[F] {
    override def stream: Stream[F, Nothing] = {
      for {
        routes   <- combinedRoutes
        app      <- httpApp(routes)
        exitCode <- Stream.resource[F, Nothing](
                      EmberServerBuilder.default[F]
                        .withHost(hostAddress)
                        .withPort(serverPort(sys.env))
                        .withHttpApp(app)
                        .build >>
                        Resource.eval(Async[F].never),
                    )
      } yield exitCode
    }.drain

    private val combinedRoutes: Stream[Pure, Http[F, F]] = {
      val healthCheck     = HealthCheck[F]
      val movieCollection = MovieCollection[F]
      val serverInfo      = ServerInfo[F]
      val routes          = Routes[F]
      val combinedRoutes  = (HttpRoutes.empty[F]
        <+> routes.imageAssetRoutes
        <+> routes.healthRoutes(healthCheck)
        <+> routes.movieRoutes(movieCollection)
        <+> routes.categoryRoutes(movieCollection)
        <+> routes.infoRoutes(serverInfo)).orNotFound
      Stream.emit[Pure, Http[F, F]](combinedRoutes)
    }

    private def httpApp(routes: Http[F, F]): Stream[Pure, HttpApp[F]] = {
      val withLogger = Logger.httpApp[F](logHeaders = true, logBody = false)(routes)
      val withCors   = CORS.policy.withAllowOriginAll.apply[F, F](withLogger)
      Stream.emit[Pure, HttpApp[F]](withCors)
    }
  }
}
