# Quote Me on Ghibli (API)

<img src="pictures/ghibli-totoro.png" alt="Ghibli Totoro" width="420px"/>

This project builds an API server that serves data on Studio Ghibli's productions.

It mainly uses the following technology stack:

- Java 17
- Scala 2
- http4s

## Endpoints

The following endpoints are available currently.

### Info

- https://quote-me-on-ghibli.herokuapp.com/info
  - return API server info

### Movies

- https://quote-me-on-ghibli.herokuapp.com/movie/all
  - return all movies
- https://quote-me-on-ghibli.herokuapp.com/movie/${ID}
  - return a specific movie
- https://quote-me-on-ghibli.herokuapp.com/movie/search
  - return movies matching any query parameter below:
    - keyword
    - categoryId
    - categoryName

### Categories

- https://quote-me-on-ghibli.herokuapp.com/category/all
  - return all categories

### Development

Use the following commands during development.

```shell
sbt clean scalafmtCheckAll compile test stage assembly run

# Alternatively, compile, stage and run.
sbt clean compile stage
./target/universal/stage/bin/quote-me-on-ghibli

# Perform integration tests against the deployed app.
sbt Gatling/test
```

#### CAVEAT

Make sure you run `sbt compile` prior to opening the project in IDE. 

Otherwise, classes produced by `sbt-buildinfo` will not be available.

### Deployment

Below are a few handy commands for deployment.

```shell
heroku login
heroku whoami

# Note that variable HEROKU_SLUG_COMMIT is no more needed as is generated
# from SOURCE_VERSION at compile time using the sbt-buildinfo plugin.
#
# For HEROKU_APP_NAME and HEROKU_SLUG_COMMIT.
heroku labs:enable runtime-dyno-metadata

# Note that the following deployment approach is not working
# due to issues within the Heroku's Scala buildpack.
#
# Deploy by git push.
# heroku git:remote --app 'quote-me-on-ghibli'
# git push 'heroku' 'main'

# Deploy with sbt-heroku.
sbt compile stage deployHeroku

# Handy commands below for troubleshooting.
heroku ps
heroku open
heroku addons
heroku config
heroku buildpacks

heroku logs --tail
heroku run 'console'
heroku local --procfile 'Procfile.windows'
```
