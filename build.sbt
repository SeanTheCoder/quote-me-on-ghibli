import com.typesafe.sbt.SbtGit.git.gitHeadCommit
import sbtassembly.AssemblyPlugin.autoImport.assembly

organization := "io.seanthecoder"
name         := "quote-me-on-ghibli"
description  := "Quote Me on Ghibli"
version      := "0.1"

scalaVersion := "2.13.8"

idePackagePrefix := "io.seanthecoder.qmog".some

val betterMonadicForVersion = "0.3.1"
val kindProjectorVersion    = "0.13.2"

val http4sVersion  = "0.23.12"
val circeVersion   = "0.14.2"
val logbackVersion = "1.2.11"

val munitVersion           = "0.7.29"
val munitCatsEffectVersion = "1.0.7"
val gatlingVersion         = "3.7.6"

addCompilerPlugin(dependency = "com.olegpy" %% "better-monadic-for" % betterMonadicForVersion)
addCompilerPlugin(dependency = "org.typelevel" % "kind-projector" % kindProjectorVersion cross CrossVersion.full)

enablePlugins(GatlingPlugin)
enablePlugins(JavaServerAppPackaging)

libraryDependencies ++= Seq(
  "org.http4s"           %% "http4s-ember-server"       % http4sVersion,
  "org.http4s"           %% "http4s-ember-client"       % http4sVersion,
  "org.http4s"           %% "http4s-circe"              % http4sVersion,
  "org.http4s"           %% "http4s-dsl"                % http4sVersion,
  "io.circe"             %% "circe-generic"             % circeVersion,
  "ch.qos.logback"        % "logback-classic"           % logbackVersion,
  "org.scalameta"        %% "munit"                     % munitVersion           % Test,
  "org.typelevel"        %% "munit-cats-effect-3"       % munitCatsEffectVersion % Test,
  "io.gatling.highcharts" % "gatling-charts-highcharts" % gatlingVersion         % Test,
  "io.gatling"            % "gatling-test-framework"    % gatlingVersion         % Test,
)

val droppedProductionWarts = Seq(
  // False positive on higher kinded typeclasses https://github.com/wartremover/wartremover/issues/463
  Wart.Any,
  Wart.DefaultArguments,
  // Allow Option#get
  Wart.OptionPartial,
)

val droppedTestWarts = Seq(
  // Allow mutability
  Wart.Var,
) ++ droppedProductionWarts

Global / excludeLintKeys ++= Set(idePackagePrefix)

Compile / compile / wartremoverErrors ++= Warts.unsafe.filterNot(droppedProductionWarts.contains)
Test / test / wartremoverErrors ++= Warts.unsafe.filterNot(droppedTestWarts.contains)

Compile / herokuAppName    := "quote-me-on-ghibli"
Compile / herokuJdkVersion := "17"

Test / parallelExecution := true

assembly / assemblyOutputPath := file("target/app.jar")
assembly / mainClass          := Some("io.seanthecoder.qmog.Main")

lazy val root = (project in file(".")).enablePlugins(BuildInfoPlugin).settings(
  buildInfoPackage := "buildInfo",
  buildInfoOptions += BuildInfoOption.BuildTime,
  buildInfoKeys    := Seq[BuildInfoKey](
    name,
    description,
    version,
    scalaVersion,
    sbtVersion,
    "gatlingVersion" -> gatlingVersion,
    BuildInfoKey.action(name = "sourceVersion") {
      gitHeadCommit.value.fold(sys.env.get("SOURCE_VERSION"))(_.some)
    },
  ),
)
